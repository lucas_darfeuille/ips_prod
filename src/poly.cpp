/*! \file poly.cpp
 * The implementation of poly.h with the use of armadillo
 */

#include "../headers/poly.h"

/**
 * Definition of the constructor of Poly
 * 
 * It does nothing, we use it juste to create the object
*/
Poly::Poly()
{
}
/**
 * Definition of the function that will calculate the Hermite polynomial 
 *
 * This function create a matrix in the Hn attribute.
 *
 * We use a recurrence relation to calculate all the values, row by row and the
 * last row is what we calculcate the matrix for.
 *
 * @param nmax the number of solutions
 * @param vectcoord an array of coordinates for which we will do the operation
 */
void Poly::calcHermite(int nmax, arma::vec vectcoord)
{
    arma::mat Ha(vectcoord.n_rows, nmax);
    Ha.col(0).ones();
    if (nmax > 1)
    {
        Ha.col(1) = 2 * vectcoord;
        for (int n = 2; n < nmax; n++)
        {
            Ha.col(n) = 2 * vectcoord % Ha.col(n - 1) - 2 * (n - 1) * Ha.col(n - 2);
        }
    }

    Hn = Ha;
}

/**
 * This function is used in order to get one column of the hermite polynomial 
 */
arma::vec Poly::hermite(int ncol)
{
    return Hn.col(ncol);
}

/**
 * Definition of the function that will calculate the Hermite polynomial 
 *
 * This constructor create an arma::cube object in LGn attribute.
 *
 * We use a recurrence relation to calculate all the values, matrix by matrix, and for each matrix we calculate them row by row.
 *
 * @param m the number of matrix calculated
 * @param nmax the number of solutions
 * @param vectcoord an array of coordinates for which we will do the operation
 */
void Poly::calcLaguerre(int mmax, int nmax, arma::vec vectcoord)
{
    arma::cube La(vectcoord.n_rows, nmax + 1, mmax + 1);

    for (int m = 0; m < mmax; m++)
    {
        arma::mat Lm(vectcoord.n_rows, nmax + 1);
        Lm.col(0).ones();
        Lm.col(1) = 1 + m - vectcoord;

        for (int n = 2; n < nmax; n++)
        {
            Lm.col(n) = (2 + (m - 1 - vectcoord) / (double)n) % Lm.col(n - 1) - (1 + (m - 1) / (double)n) * Lm.col(n - 2);
        }

        La.slice(m) = Lm;
    }

    LGn = La;
}

/**
 * This function is used in order to get one column of the Laguerre polynomial 
 */

arma::vec Poly::laguerre(int m, int n)
{
    return LGn.slice(m).col(n);
}