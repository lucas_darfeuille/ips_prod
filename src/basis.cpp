/*! \file basis.cpp
 * The implementation of the basis.h file. It is used to calculate the basis truncation, the psi-function, and rParts/zParts which are needed for the psi-function
 */

#include "../headers/basis.h"
#include "../headers/poly.h"
#include <stdlib.h>
#include <stdio.h>

/// @brief An auxiliary function used to get the value of the nu-function
/// @param i the variable in the nu-function. It must be an int
/// @param N a parameter of the nu-function. It must be an int
/// @param Q a parameter f the nu-function. It is a real number
/// @return the value of the nu-function for i, N and Q
double calcV(int i, int N, double Q) {
    return (N + 2) * pow(Q, 0.6666) + 0.5 - (double)i * Q;
}

/// @brief Generate the basis truncation
/// @param br a basis deformation parameter
/// @param bz a basis deformation parameter
/// @param N The int parameter used for the nu-function
/// @param Q The real parameter used for the nu-function
Basis::Basis(double br, double bz, int N, double Q ){
    Basis::br = br;
    Basis::bz = bz;

    /// Here is defined mMax
    int i = 0;
    mMax = 0;
    double v;
    do
    {
        v = calcV(i, N, Q);

        if (v >= 1)
        {
            mMax = i;
        }
        i += 1;
    } while (v >= 1); /// This condition will be met because the limit of v is -infinity

    arma::ivec calc_nMax(mMax);
    /// here is defined nMax
    for (int m = 0; m < mMax; m += 1)
    {
        calc_nMax(m) = 0.5 * (mMax - m - 1) + 1;
    }
    nMax = calc_nMax;

    arma::imat calc_n_zMax(mMax, nMax(0));
    /// here is defined n_zMax
    for (int m = 0; m < mMax; m += 1)
    {
        for (int n = 0; n < nMax(m); n += 1)
        {
            calc_n_zMax(m, n) = calcV(m + 2 * n + 1, N, Q);
        }
    }
    n_zMax = calc_n_zMax;
}

/// @brief This function is used to calculate the r-Part of the psi-function
/// @param r a vector of radius values
/// @param m a quantum number
/// @param n a quantum number
/// @return a vector containing the r-parts of the psi-function
arma::vec Basis::rPart(arma::vec r, int m, int n)
{
    int len = r.n_rows;
    double br2 = Basis::br * Basis::br;
    arma::vec res(len);
    arma::vec vect_aux1(len);
    arma::vec vect_aux2(len);
    double cst = 1.0 / (Basis::br * sqrt(arma::datum::pi));
    double cst_fact = 1.0;
    /// Here is defined the constant based on the factorial
    for (int i = n + 1; i <= n + abs(m); i++)
    {
        cst_fact *= sqrt((double)1 / i);
    }
    res = (-1.0 / (2 * br2)) * r % r;
    res = arma::exp(res);
    vect_aux1 = arma::pow((1.0 / Basis::br) * r, m);
    vect_aux2 = r % r / br2;
    Poly p;
    p.calcLaguerre(m + 1, n + 1, vect_aux2);
    return cst * cst_fact * res % vect_aux1 % p.laguerre(m, n);
}

/// @brief This function is used to calculate the z-Part of the psi-function
/// @param z a vector of z-axis values
/// @param nz a quantum number
/// @return a vector containing the z-parts of the psi-function
arma::vec Basis::zPart(arma::vec z, int nz)
{
    double cst_calc = pow(arma::datum::pi, -0.25);
    int len = z.n_rows;
    arma::vec r_herm(len);
    for (int i = 1; i <= nz; i++)
    {
        cst_calc *= pow(2 * i, -0.5);
    }
    r_herm = z / Basis::bz;
    Poly p;
    p.calcHermite(nz + 1, r_herm);
    return (1.0 / sqrt(Basis::bz)) * cst_calc * arma::exp((-1.0 / (2 * Basis::bz * Basis::bz)) * z % z) % p.hermite(nz);
}

/// @brief This is used to calculate the psi-function on all couple (r,z) given in the parameters zVals and rVals
/// @param m a quantum number
/// @param n a quantum number
/// @param n_z a quantum number
/// @param zVals the set of values on the z-axis 
/// @param rVals the set of radius values
/// @return a matrix full of psi(r,z) for all possible couple
arma::mat Basis::psi(int m, int n, int n_z, arma::vec zVals, arma::vec rVals)
{
    return Basis::rPart(rVals, m, n) * Basis::zPart(zVals, n_z).t();
}
