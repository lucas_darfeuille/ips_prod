/*! \file main.cpp
 * The main function executed by a user. It calculate the local density of a nuclear system.
 * The user will get the speedup of our optimization, a .csv to be ploted and a .df3 to be visualized.
 */

#include <iostream>
#include <string>
#include <armadillo>
#include <time.h>
#include "../headers/basis.h"
using namespace std;

arma::mat rho;
Basis basis(1.935801664793151, 2.829683956491218, 14, 1.3);

arma::icube rho_indice = arma::icube(basis.mMax, basis.nMax(0), basis.n_zMax(0, 0));

/** \details This function is the naive algorithm given in the project presentation
 */
arma::mat naiv_calc(arma::vec r, arma::vec z)
{

    int a = 0, b = 0;
    arma::mat result = arma::zeros(r.n_rows, z.n_rows);
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat psiA = basis.psi(m, n, n_z, z, r);
                            arma::mat psiB = basis.psi(mp, np, n_zp, z, r);

                            int i;
                            i = 0;
                            for (int m_loop = 0; m_loop < basis.mMax; m_loop++)
                            {
                                for (int n_loop = 0; n_loop < basis.nMax(m_loop); n_loop++)
                                {
                                    for (int n_z_loop = 0; n_z_loop < basis.n_zMax(m_loop, n_loop); n_z_loop++)
                                    {
                                        if (m_loop == m && n_loop == n && n_z_loop == n_z)
                                        {
                                            a = i;
                                        }
                                        i++;
                                    }
                                }
                            }

                            i = 0;
                            for (int m_loop = 0; m_loop < basis.mMax; m_loop++)
                            {
                                for (int n_loop = 0; n_loop < basis.nMax(m_loop); n_loop++)
                                {
                                    for (int n_z_loop = 0; n_z_loop < basis.n_zMax(m_loop, n_loop); n_z_loop++)
                                    {
                                        if (m_loop == mp && n_loop == np && n_z_loop == n_zp)
                                        {
                                            b = i;
                                        }
                                        i++;
                                    }
                                }
                            }

                            result += psiA % psiB * rho(a, b);
                        }
                    }
                }
            }
        }
    }
    return result;
}

/** \details This function is the first optimization of the naive algorithm, just by moving where we calculate psiA
 */
arma::mat first_calc(arma::vec r, arma::vec z)
{
    int a, b;

    arma::mat result = arma::zeros(r.n_rows, z.n_rows);
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m, n, n_z, z, r);

                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat psiB = basis.psi(mp, np, n_zp, z, r);

                            a = rho_indice(m, n, n_z);
                            b = rho_indice(mp, np, n_zp);

                            result += psiA % psiB * rho(a, b);
                        }
                    }
                }
            }
        }
    }

    return result;
}

/** \details This function is the second optimization of the naive algorithm, we replace mp by m
 */
arma::mat second_calc(arma::vec r, arma::vec z)
{

    int a, b;

    arma::mat result = arma::zeros(r.n_rows, z.n_rows);
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m, n, n_z, z, r);

                for (int np = 0; np < basis.nMax(m); np++)
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {
                        arma::mat psiB = basis.psi(m, np, n_zp, z, r);

                        a = rho_indice(m, n, n_z);
                        b = rho_indice(m, np, n_zp);
                        result += psiA % psiB * rho(a, b);
                    }
                }
            }
        }
    }

    return result;
}

/** \details This function is the third optimization of the naive algorithm, we move where we calculate the first indice of rho
 */
arma::mat third_calc(arma::vec r, arma::vec z)
{

    int a, b;

    arma::mat result = arma::zeros(r.n_rows, z.n_rows);
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m, n, n_z, z, r);
                a = rho_indice(m, n, n_z);

                for (int np = 0; np < basis.nMax(m); np++)
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {
                        arma::mat psiB = basis.psi(m, np, n_zp, z, r);

                        b = rho_indice(m, np, n_zp);
                        result += psiA % psiB * rho(a, b);
                    }
                }
            }
        }
    }

    return result;
}

/** \details This function is the fourth optimization of the naive algorithm, we optimized using the symetry
 */
arma::mat fourth_calc(arma::vec r, arma::vec z)
{
    int a, b;

    arma::mat result = arma::zeros(r.n_rows, z.n_rows);
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m, n, n_z, z, r);
                a = rho_indice(m, n, n_z);

                for (int np = 0; np < basis.nMax(m); np++)
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {

                        b = rho_indice(m, np, n_zp);
                        if (a < b)
                        {
                            arma::mat psiB = basis.psi(m, np, n_zp, z, r);

                            result += 2 * (psiA % psiB * rho(a, b));
                        }
                        else if (a == b)
                        {
                            arma::mat psiB = basis.psi(m, np, n_zp, z, r);

                            result += psiA % psiB * rho(a, b);
                        }
                    }
                }
            }
        }
    }

    return result;
}

/** \details This function is the final version of the algorithm
 */
arma::mat last_calc(arma::vec r, arma::vec z)
{

    int a, b;

    arma::mat result_part1 = arma::zeros(r.n_rows, z.n_rows);
    arma::mat result_part2 = arma::zeros(r.n_rows, z.n_rows);
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m, n, n_z, z, r);
                a = rho_indice(m, n, n_z);

                for (int np = 0; np < basis.nMax(m); np++)
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {

                        b = rho_indice(m, np, n_zp);
                        if (a < b)
                        {
                            arma::mat psiB = basis.psi(m, np, n_zp, z, r);

                            result_part1 += psiA % psiB * rho(a, b);
                        }
                        else if (a == b)
                        {
                            arma::mat psiB = basis.psi(m, np, n_zp, z, r);

                            result_part2 += psiA % psiB * rho(a, b);
                        }
                    }
                }
            }
        }
    }

    return (2 * result_part1 + result_part2);
}

/** \details This function is given in the project presentation, it translate a arma::cube object to create a df3 format string to visualize our result
 */
std::string cubeToDf3(const arma::cube &m)
{
    std::stringstream ss(std::stringstream::out | std::stringstream::binary);
    int nx = m.n_rows;
    int ny = m.n_slices;
    int nz = m.n_cols;
    ss.put(nx >> 8);
    ss.put(nx & 0xff);
    ss.put(ny >> 8);
    ss.put(ny & 0xff);
    ss.put(nz >> 8);
    ss.put(nz & 0xff);
    double theMin = 0.0;
    double theMax = m.max();
    for (uint j = 0; j < m.n_cols; j++)
    {
        for (uint k = 0; k < m.n_slices; k++)
        {

            for (uint i = 0; i < m.n_rows; i++)
            {
                uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
                ss.put(v);
            }
        }
    }

    return ss.str();
}

/** \details This main will compare all the optimization and give us the speed up at the end.  
 *  It also creates a .csv for the 2D plot and a .df3 file used to visualize in 3D.
 */

int main(int argc, char const *argv[])
{
    struct timespec t0, t1;
    double timeNaive, time1, time2, time3, time4, time5;

    rho.load("rho.arma", arma::arma_ascii);
    uint i = 0;
    for (int m = 0; m < basis.mMax; m++)
        for (int n = 0; n < basis.nMax(m); n++)
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                rho_indice(m, n, n_z) = i;
                i++;
            }

    arma::vec r = {3.1, 2.3, 1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
    arma::vec z = {-10.1, -8.4, -1.0, 0.0, 0.1, 4.3, 9.2, 13.7};

    clock_gettime(CLOCK_REALTIME, &t0);
    arma::mat result_naiv = naiv_calc(r, z);
    clock_gettime(CLOCK_REALTIME, &t1);
    timeNaive = (1000. * (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / 1000000.) / 1000.;
    printf("Time for the naive  implementation %fs \n", timeNaive);

    clock_gettime(CLOCK_REALTIME, &t0);
    arma::mat result_first = first_calc(r, z);
    clock_gettime(CLOCK_REALTIME, &t1);
    time1 = (1000. * (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / 1000000.) / 1000.;
    printf("Time for the first  implementation %fs\n", time1);

    clock_gettime(CLOCK_REALTIME, &t0);
    arma::mat result_second = second_calc(r, z);
    clock_gettime(CLOCK_REALTIME, &t1);
    time2 = (1000. * (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / 1000000.) / 1000.;
    printf("Time for the second implementation %fs \n", time2);

    clock_gettime(CLOCK_REALTIME, &t0);
    arma::mat result_third = third_calc(r, z);
    clock_gettime(CLOCK_REALTIME, &t1);
    time3 = (1000. * (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / 1000000.) / 1000.;
    printf("Time for the third  implementation %fs \n", time3);

    clock_gettime(CLOCK_REALTIME, &t0);
    arma::mat result_fourth = fourth_calc(r, z);
    clock_gettime(CLOCK_REALTIME, &t1);
    time4 = (1000. * (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / 1000000.) / 1000.;
    printf("Time for the fourth implementation %fs \n", time4);

    clock_gettime(CLOCK_REALTIME, &t0);
    arma::mat result_fifth = last_calc(r, z);
    clock_gettime(CLOCK_REALTIME, &t1);
    time5 = (1000. * (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec) / 1000000.) / 1000.;
    printf("Time for the last   implementation %fs \n", time5);

    printf("Total Speed-up : %f %% \n", (timeNaive / time5) * 100);

    arma::vec r_calc = arma::vec(251).zeros();
    arma::vec z_calc = arma::vec(501).zeros();
    arma::vec x_calc = arma::vec(500);

    double h = 0.1;
    for (int i = 1; i < 251; i++)
    {
        r_calc(i) = r_calc(i - 1) + h;
        z_calc(250 + i) = z_calc(250 + i - 1) + h;
        z_calc(250 - i) = z_calc(250 - i + 1) - h;
    }

    arma::mat result = last_calc(r_calc, z_calc);

    arma::mat result_2D = arma::join_cols(arma::reverse(result.submat(1, 0, 250, 500)), result);


    double min_2D = 0.0;
    double max_2D = result_2D.max();

    result_2D.transform(([min_2D, max_2D](double val)
                         { return 255 * (val - min_2D) / (max_2D - min_2D); }));


    result_2D.save("result.csv", arma::csv_ascii);

    arma::vec x_3D_plot = arma::vec(32);
    arma::vec y_3D_plot = arma::vec(32);
    arma::vec z_3D_plot = arma::vec(64);

    x_3D_plot(0) = -1 * 10;
    y_3D_plot(0) = -1 * 10;
    z_3D_plot(0) = -2 * 10;
    z_3D_plot(63) = 2 * 10;
    double h_xy = (2. / (32. - 1.)) * 10;
    double h_z = (4. / (64. - 1.)) * 10;
    for (int i = 1; i < 32; i++)
    {
        x_3D_plot(i) = x_3D_plot(i - 1) + h_xy;
        y_3D_plot(i) = y_3D_plot(i - 1) + h_xy;
        z_3D_plot(2 * i - 1) = z_3D_plot(2 * i - 2) + h_z;
        z_3D_plot(2 * i) = z_3D_plot(2 * i - 1) + h_z;
    }

    arma::mat y_squared_3D = y_3D_plot;
    y_squared_3D.transform([](double val)
                           { return (val * val); });

    arma::cube m = arma::cube(32, 64, 32);
    arma::vec r_3D_plot = arma::vec(32);

    for (int i = 0; i < 32; i++)
    {
        r_3D_plot = x_3D_plot(i) * x_3D_plot(i) + y_squared_3D;

        r_3D_plot.transform(([](double val)
                             { return (pow(val, 0.5)); }));

        m.slice(i) = last_calc(r_3D_plot, z_3D_plot);
    }



    //To plot the sphere: povray +A0.0001 -W800 -H600 +P +Q11 visu.pov 
    std::string input = cubeToDf3(m);
    std::ofstream out("sphere.df3");
    out << input;
    out.close();

    return 0;
}
