/*! \file basis.h
 *  Here is defined he Basis class. It is used to calculate the basis truncation, the psi-function, and rParts/zParts which are needed for the psi-function
 */

#include <armadillo>

class Basis
{
public:
    int mMax;
    double bz;
    double br;
    arma::ivec nMax;
    arma::imat n_zMax;

    /** Constructor of Basis class */
    Basis(double br, double bz, int N, double Q);
    /** This function is used to calculate the r part of the psi function */
    arma::vec rPart(arma::vec r, int m, int n);
    /** This function is used to calculate the z part of the psi function */
    arma::vec zPart(arma::vec z, int nz);
    /** This function is used to calculate the psi-function on all couple (r,z) given in the parameters zVals and rVals */
    arma::mat psi(int m, int n, int n_z, arma::vec zVals, arma::vec rVals);
};