/*! \file poly.h
 * Here is defined the Poly class, used to calculate Laguerre and Hermite polynomials
 */
#include <armadillo>

/** \details
 * Here is defined the Poly class, used to calculate Laguerre and Hermite polynomials
 */
class Poly
{
private:
    /** The attribute that will contains value for Hermite polynomials */
    arma::mat Hn;
    /** The attribute that will contains value for Laguerre polynomials */
    arma::cube LGn;

public:
    /** Constructor of the Poly class */
    Poly();
    /** The functions used in order to set up and get Hermite's polynomials */
    void calcHermite(int nmax, arma::vec vectcoord);
    arma::vec hermite(int nrow);
    /** The functions used in order to set up and get Laguerre's polynomials */
    void calcLaguerre(int m, int nmax, arma::vec vectcoord);
    arma::vec laguerre(int m, int n);
};
