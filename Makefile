CXX = g++
CXXFLAGS = -Wall -O3 -std=c++11
LDFLAGS =
LIBS = -larmadillo
TARGET = bin/main
OBJS = obj/main.o obj/basis.o obj/poly.o
CXXTEST = cxxtestgen
CXXTESTFLAGS = --error-printer 
TARGET_TEST = bin/test
TESTS = test/test_mandatory.h
OBJS_TEST =  obj/test.o obj/basis.o obj/poly.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@ $(LIBS)

obj/%.o: src/%.cpp headers/%.h
	$(CXX) $(CXXFLAGS) -c $< -o $@

obj/main.o: src/main.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@


.PHONY: clean
clean:
	rm -f $(OBJS)
	rm -f $(OBJS_TEST)
	rm -f $(TARGET)
	rm -f $(TARGET_TEST)

test: $(TARGET_TEST) 
	rm -f test/test.cpp

$(TARGET_TEST): $(OBJS_TEST)
	$(CXX) $(LDFLAGS) $^ -o $@ $(LIBS)

obj/test.o: test/test.cpp 
	$(CXX) $(CXXFLAGS) -c $< -o $@

test/%.cpp : $(TESTS)
	$(CXXTEST) $(CXXTESTFLAGS) -o $@ $<
